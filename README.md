Fichiers customs utilisés pour l'instance Pleroma [soc.emelyne.eu](https://soc.emelyne.eu/)

La licence applicable est la [CC0](https://creativecommons.org/publicdomain/zero/1.0/), sauf exceptions listées ci dessous.

Crédits :

- Emojis :
    - Emojis packs :
        - [Rats Emojis Pack](https://labo.emelyne.eu/oniricorpe/blobrats) by [@AuroreAcelya@pipou.academy](https://pipou.academy/@AuroreAcelya) (**CC0**)
            - `blobrat/*.png`
        - [Lila's Emojis Pack](https://labo.emelyne.eu/oniricorpe/lila) by [@lila_bliblu@mastodon.opportunis.me](https://mastodon.opportunis.me/@lila_bliblu/100374472694356312) (**CC BY NC**)
            - `lila/*.png`
        - [Blobkoalas Emojis Pack](https://labo.lacoloc.cafe/june/blobkoalas) by [@Hamano@social.lacoloc.cafe](https://social.lacoloc.cafe/Hamano) (**CC0**)
            - `blobkoala/*.png`
        -  [Blobfox Emojis Pack](https://www.feuerfuchs.dev/projects/blobfox-emojis/) (**Apache 2.0**)
            - `blobfox/*.png`
        -  [Buns Emojis Pack](https://git.pleroma.social/pleroma/emoji-index/) (**Unknown licence**)
            - `buns/*.png`
    - [Blobs](https://blobs.gg/) (**Apache 2.0**)
        - `blobs/*.*`
    - [Mutant](https://mutant.tech/) (**CC BY-NC-SA 4.0**)
        - `mutant/*.png`

- Thèmes :
    - [Vulpes One](https://plthemes.vulpes.one/themes/vulpes-one/) (modified) : by [Feuerfuchs](https://fedi.absturztau.be/users/Feuerfuchs) (**Unknown license**)

- Font :
    - [B612](https://b612-font.com/) : by [PolarSys](https://github.com/polarsys/b612) (**SIL Open Font License**)
